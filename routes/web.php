<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('iniciar-sesion/{tipo}', 'IniciarSesionController@VistaIniciarSesion')->name('iniciar-sesion');
Route::post('iniciar-sesion/presentar', 'IniciarSesionController@IniciarSesionPresentar')->name('iniciar-sesion/presentar');
Route::post('iniciar-sesion/administrar', 'IniciarSesionController@IniciarSesionAdministrar')->name('iniciar-sesion/administrar');
Route::get('cerrar-sesion', 'IniciarSesionController@CerrarSesion')->name('cerrar-sesion');
Route::post('iniciar-sesion/obtener-clientes', 'IniciarSesionController@ObtenerClientes')->name('iniciar-sesion/obtener-clientes');

Auth::routes();

Route::group(['middleware' => ['auth', 'permissions']], function () {
    Route::get('/', 'InicioController@index')->name('/');
    Route::get('inicio/{modulo_id}', 'InicioController@seleccionarModulo')->name('inicio');
    Route::prefix('configuracion')->group(function () {
        Route::post('cliente/exportar/{tipo}', 'Configuracion\ClienteController@export')->name('cliente/exportar');
        Route::resource('cliente', 'Configuracion\ClienteController', ['names' => 'cliente']);
        Route::resource('usuario', 'Configuracion\UsuarioController', ['names' => 'usuario']);
        Route::resource('rol', 'Configuracion\RoleController', ['names' => 'rol']);
        Route::resource('caracteristica', 'Configuracion\CaracteristicasController', ['names' => 'caracteristica']);
    });

    Route::prefix('clima-cultura-organizacional')->group(function () {
        Route::resource('proceso', 'Proceso\ProcesoController', ['names' => 'proceso']);
        Route::resource('instrumento', 'ClimaCulturaOrganizacional\InstrumentoController', ['names' => 'instrumento']);
        Route::resource('presentar-encuesta', 'ClimaCulturaOrganizacional\PresentarClimaCulturaController', ['names' => 'presentar-encuesta']);
        Route::resource('reporte', 'ClimaCulturaOrganizacional\ReporteController', ['names' => 'reporte']);
    });
});
