<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ParticipanteCaracteristica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participantes_caracteristicas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('participante_id')->constrained();
            $table->foreignId('caracteristica_id')->constrained();
            $table->string('valor', 250);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parcipantes_caracteristicas');
    }
}
