@extends('plantilla.correo.base')

@section('contenido')
Hola {{$name}}, a traves de este correo queremos informate que has sido registrado.
<br>
<br>
Tus datos de acceso son los siguientes:
<br>
<b>usuario:</b> {{$numero_documento}}
<br>
<b>contraseña:</b> {{$numero_documento}}
<br>
<br>
Haciendo <a href="{{url('clima-cultura-organizacional/presentar-encuesta')}}">Clic aqui</a> podras ir al formulario de inicio de sesion. Te esperamos!
@endsection
