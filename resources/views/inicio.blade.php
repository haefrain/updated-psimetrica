@extends('plantilla.administrador.base')

@section('titulo', 'Psimetrica - Inicio')

@section('javascript')
@endsection

@section('contenido')
    @foreach($modulos AS $modulo)
        <div class="col-md-4">
            <a href="{{route('inicio', $modulo->id)}}">
                <div class="xe-height-custom xe-widget xe-vertical-counter xe-vertical-counter-{{$modulo->color}}">
                    <div class="xe-icon">
                    <i class="fa fa-{{$modulo->icono}}"></i>
                    </div>

                    <div class="xe-label">
                        <h4>
                            <b>{{$modulo->nombre}}</b>
                        </h4>
                        <small data-toggle="tooltip" data-placement="top" data-original-title="{{$modulo->descripcion}}">{{substr($modulo->descripcion, 0, 100).'...'}}</small>
                    </div>
                </div>
            </a>
        </div>
    @endforeach
@endsection
