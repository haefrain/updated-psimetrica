@extends('plantilla.administrador.base')

@section('titulo', 'Psimetrica - Inicio')

@section('javascript')
<script>
    $(document).ready(function () {
        let bloqueEncuesta = 0
        $('#introduccion').show();
        $('#datos_personales').hide();
        $('#datos_sociodemograficos').hide();
        $('#encuesta').hide();
        $('.bloques-visibles').hide();
        $('#bloque-' + bloqueEncuesta).fadeIn();

        $("#abrir_modal").click( () => {
            $("#modal_politicas").modal('show', {backdrop: 'fade'});
        });

        $("#boton_introduccion").click(function () {
            $('#introduccion').hide();
            $('#datos_personales').show();
        });

        $("#boton_informacion_basica").click(function () {
            let cantidadErrores = 0
            $.each($('#datos_personales div div input, #datos_personales div div select'), function (index, input) {
                if (input.value === '') {
                    cantidadErrores = parseInt(cantidadErrores) + 1
                    $("#" + input.id +"--error").show();
                }
            });

            if (cantidadErrores > 0) {
                return false
            }
            $('#datos_personales').hide();
            $('#datos_sociodemograficos').show();
        });

        $("#boton_informacion_sociodemografica").click(function () {
            let cantidadErrores = 0
            $.each($('#datos_sociodemograficos div div input, #datos_sociodemograficos div div select'), function (index, input) {
                if (input.value === '') {
                    cantidadErrores = parseInt(cantidadErrores) + 1
                    $("#" + input.id +"--error").show();
                }
            });

            if (cantidadErrores > 0) {
                return false
            }
            $('#datos_sociodemograficos').hide();
            $('#encuesta').show();
        });

        $(".guardar_cambiar_caracteristica").blur(function () {
            if ($(this).val() === '') {
                $("#"+$(this).attr('id') +"--error").show();
                return false
            }
            let tipo = 'caracteristica'
            let metodo = 'put'
            const datos = {
                _token: "{{ csrf_token() }}",
                _method: metodo,
                campo: $(this).attr('name'),
                valor: $(this).val(),
            }
            $.ajax({
                url : "{{url('clima-cultura-organizacional/presentar-encuesta')}}/" + tipo,
                data : datos,
                type : 'POST',
                dataType : 'json',
                success : function(json) {
                    $("#"+$(this).attr('id') +"--error").hide();
                },
                error : function(xhr, status) {
                },
                complete : function(xhr, status) {
                }
            });
        })

        $("#boton_encuesta_atras").click(function () {
            if (bloqueEncuesta <= 0) {
                $("#boton_encuesta_atras").attr('disabled', true);
            } else {
                $("#boton_encuesta_atras").attr('disabled', false);
                $("#bloque-"+bloqueEncuesta).hide();
                bloqueEncuesta = parseInt(bloqueEncuesta) - 1;
                $("#bloque-"+bloqueEncuesta).show();

                if (bloqueEncuesta <= 0) {
                    $("#boton_encuesta_atras").attr('disabled', true);
                }
            }
        });

        $("#boton_encuesta_continuar").click(function () {
            let respuestaPregunta = false
            let contadorErrores = 0
            let contador = 0
            $.each($("#encuesta #bloque-"+bloqueEncuesta+" tr td input"), function(index, input) {
                if (
                    $("#"+input.name+"-1").is(':checked') ||
                    $("#"+input.name+"-2").is(':checked') ||
                    $("#"+input.name+"-3").is(':checked') ||
                    $("#"+input.name+"-4").is(':checked') ||
                    $("#"+input.name+"-5").is(':checked')
                ) {
                    $("#error-"+input.name).hide()
                } else {
                    contadorErrores = parseInt(contadorErrores) + 1
                    $("#error-"+input.name).show()
                }
            });

            if (contadorErrores > 0) {
                return false
            }


            if (bloqueEncuesta <= 0) {
                $("#boton_encuesta_atras").attr('disabled', true);
            }
            $("#boton_encuesta_atras").attr('disabled', false);
            $("#bloque-"+bloqueEncuesta).hide();
            bloqueEncuesta = parseInt(bloqueEncuesta) + 1;
            $("#bloque-"+bloqueEncuesta).show();

            if ($("#bloque-"+bloqueEncuesta).attr('tag') == 'mensaje_finalizacion') {
                $("#contenedor_botones_encuesta, #titulo_encuesta").hide();
            }
        });

        $(".guardar_cambiar_encuesta").click(function () {
            let metodo = 'post'
            const datos = {
                _token: "{{ csrf_token() }}",
                _method: metodo,
                campo: $(this).attr('tag'),
                valor: $(this).val(),
            }
            $("#error-"+$(this).attr('name')).hide();
            $.ajax({
                url : "{{url('clima-cultura-organizacional/presentar-encuesta')}}",
                data : datos,
                type : 'POST',
                dataType : 'json',
                success : function(json) {

                },
                error : function(xhr, status) {
                },
                complete : function(xhr, status) {
                }
            });
        })

    });
</script>
@endsection

@section('contenido')
    <div class="row col-md-8 col-md-offset-2" id="introduccion">
        <div class="row">
            <div class="col-md-4">
                @if(session('cliente')->logo !== '')
                <img src="{{asset('imagenes/logos/'.session('cliente')->logo)}}">
                @else
                    <img src="{{asset('imagenes/default/logo.png')}}">
                @endif
            </div>
            <div class="col-md-4 text-center">
                <h2>Encuesta Online <br> <br> Encuesta de cultura organizacional</h2>
            </div>
            <div class="col-md-4">
                <img src="{{asset('imagenes/logos/salud.png')}}">
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <button id="abrir_modal" class="btn btn-blue">Politica de seguridad de la informacion</button>
            </div>
            <div class="col-md-12" style="text-align:justify">
                <br> <br>El desarrollo de la encuesta no le tomará más de 30 minutos, sus respuestas serán de carácter confidencial y utilizadas únicamente para este fin.
                <br> <br>Tenga en cuenta que hay una sola opción de respuesta por pregunta. De manera obligatoria se debe responder cada pregunta, para avanzar.
                <br> <br>Es importante responder de manera franca y honesta, ya que, esta información será tenida en cuenta en el plan de acción.
                <br> <br>La encuesta estará disponible del <b>2 al 13 de abril del 2020.</b>
            </div>

            <div class="col-md-12" style="text-align:justify">
                <br> <br>
                <p>
                    A continuación encontrará una serie de preguntas que nos permitirán conocer su percepción acerca de diferentes aspectos relacionados con La Subred Sur Occidente E.S.E.  y de los cuáles es importante conocer su opinión. Para esto, lo invitamos a responder a cada ítem la opción que considere más adecuada según su percepción, focalice su respuesta en lo que realmente pasa y no en lo que usted cree o piensa que debe ser. Las respuestas no serán de conocimiento individual, si no, que harán parte de los resultados globales del estudio.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button class="btn btn-danger pull-right" id="boton_introduccion">Continuar</button>
            </div>
        </div>
    </div>
    <div class="row" id="datos_personales">
        <div class="row col-md-8 col-md-offset-2">
                <div class="form-group col-md-6">
                    <label for="identificacion">identificacion:</label>
                    <span style="display:none; color:red;" id="identificacion--error">Por favor ingrese un valor</span>
                    <input type="number" class="form-control guardar_cambiar_caracteristica" id="identificacion" name="identificacion"  value="{{$caracteristicas_valor['identificacion']}}" autocomplete="nope">
                </div>

                <div class="form-group col-md-6">
                    <label for="nombre_apellidos">Nombres y apellidos:</label>
                    <span style="display:none; color:red;" id="nombre_apellidos--error">Por favor ingrese un valor</span>
                    <input type="text" class="form-control guardar_cambiar_caracteristica" id="nombre_apellidos" name="nombre_apellidos"  value="{{$caracteristicas_valor['nombre_apellidos']}}" autocomplete="nope">
                </div>

                <div class="form-group col-md-6">
                    <label for="correo">Correo electronico:</label>
                    <span style="display:none; color:red;" id="correo--error">Por favor ingrese un valor</span>
                    <input type="email" class="form-control guardar_cambiar_caracteristica" id="correo" name="correo"  value="{{$caracteristicas_valor['correo']}}" autocomplete="nope">
                </div>

                <div class="form-group col-md-6">
                    <label for="proceso_pertenece">Proceso al que pertenece:</label>
                    <span style="display:none; color:red;" id="proceso_pertenece--error">Por favor ingrese un valor</span>
                    <select class="form-control guardar_cambiar_caracteristica" id="proceso_pertenece" name="proceso_pertenece">
                        <option value="">Seleccione una opcion</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina Asesora Jurídica' ? 'selected' : ''}} value="Oficina Asesora Jurídica">Oficina Asesora Jurídica</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina Asesora de Desarrollo Institucional' ? 'selected' : ''}} value="Oficina Asesora de Desarrollo Institucional">Oficina Asesora de Desarrollo Institucional</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina Asesora de Comunicaciones' ? 'selected' : ''}} value="Oficina Asesora de Comunicaciones">Oficina Asesora de Comunicaciones</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina Control Interno Disciplinario' ? 'selected' : ''}} value="Oficina Control Interno Disciplinario">Oficina Control Interno Disciplinario</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina Asesora de Calidad y Mejoramiento' ? 'selected' : ''}} value="Oficina Asesora de Calidad y Mejoramiento">Oficina Asesora de Calidad y Mejoramiento</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina de Sistemas de Información TICS' ? 'selected' : ''}} value="Oficina de Sistemas de Información TICS">Oficina de Sistemas de Información TICS</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina de Control Interno' ? 'selected' : ''}} value="Oficina de Control Interno">Oficina de Control Interno</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina de Participación Ciudadana y Servicio al Ciudadano' ? 'selected' : ''}} value="Oficina de Participación Ciudadana y Servicio al Ciudadano">Oficina de Participación Ciudadana y Servicio al Ciudadano</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Oficina de Gestión del Conocimiento' ? 'selected' : ''}} value="Oficina de Gestión del Conocimiento">Oficina de Gestión del Conocimiento</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Servicios Ambulatorios' ? 'selected' : ''}} value="Dirección de Servicios Ambulatorios">Dirección de Servicios Ambulatorios</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Servicios Hospitalarios' ? 'selected' : ''}} value="Dirección de Servicios Hospitalarios">Dirección de Servicios Hospitalarios</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Servicios de Urgencias' ? 'selected' : ''}} value="Dirección de Servicios de Urgencias">Dirección de Servicios de Urgencias</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Servicios Complementarios' ? 'selected' : ''}} value="Dirección de Servicios Complementarios">Dirección de Servicios Complementarios</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Gestión en Riesgo en Salud' ? 'selected' : ''}} value="Dirección de Gestión en Riesgo en Salud">Dirección de Gestión en Riesgo en Salud</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Gestión del Riesgo' ? 'selected' : ''}} value="Dirección de Gestión del Riesgo">Dirección de Gestión del Riesgo</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección Financiera' ? 'selected' : ''}} value="Dirección Financiera">Dirección Financiera</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección Administrativa' ? 'selected' : ''}} value="Dirección Administrativa">Dirección Administrativa</option>
                        <option {{$caracteristicas_valor['proceso_pertenece'] == 'Dirección de Contratación' ? 'selected' : ''}} value="Dirección de Contratación">Dirección de Contratación </option>
                    </select>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-danger pull-right" id="boton_informacion_basica">Continuar</button>
                </div>
        </div>
    </div>
    <div class="row" id="datos_sociodemograficos">
        <div class="row col-md-10 col-md-offset-1">
            <div class="form-group col-md-4">
                <label for="fecha_nacimiento">Fecha nacimiento:</label>
                <span style="display:none; color:red;" id="fecha_nacimiento--error">Por favor ingrese un valor</span>
                <input type="date" class="form-control guardar_cambiar_caracteristica" id="fecha_nacimiento" name="fecha_nacimiento"  value="{{$caracteristicas_valor['fecha_nacimiento']}}" autocomplete="nope">
            </div>

            <div class="form-group col-md-4">
                <label for="genero">Genero:</label>
                <span style="display:none; color:red;" id="genero--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="genero" name="genero">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['genero'] == 'Femenino' ? 'selected' : ''}} value="Femenino">Femenino</option>
                    <option {{$caracteristicas_valor['genero'] == 'Masculino' ? 'selected' : ''}} value="Masculino">Masculino</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="estado_civil">Estado civil:</label>
                <span style="display:none; color:red;" id="estado_civil--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="estado_civil" name="estado_civil">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['estado_civil'] == 'Soltero' ? 'selected' : ''}} value="Soltero">Soltero</option>
                    <option {{$caracteristicas_valor['estado_civil'] == 'Casado' ? 'selected' : ''}} value="Casado">Casado</option>
                    <option {{$caracteristicas_valor['estado_civil'] == 'Union libre' ? 'selected' : ''}} value="Union libre">Union libre</option>
                    <option {{$caracteristicas_valor['estado_civil'] == 'Viudo' ? 'selected' : ''}} value="Viudo">Viudo</option>
                    <option {{$caracteristicas_valor['estado_civil'] == 'Separado' ? 'selected' : ''}} value="Separado">Separado</option>
                </select>
            </div>

            <div class="form-group col-md-4">
                <label for="tiene_hijos">¿Tiene hijos?</label>
                <span style="display:none; color:red;" id="tiene_hijos--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="tiene_hijos" name="tiene_hijos">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['tiene_hijos'] == 'Si' ? 'selected' : '' }} value="Si">Si</option>
                    <option {{$caracteristicas_valor['tiene_hijos'] == 'No' ? 'selected' : '' }} value="No">No</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="cuantos_hijos">¿Cuantos?</label>
                <span style="display:none; color:red;" id="cuantos_hijos--error">Por favor ingrese un valor</span>
                <input type="number" class="form-control guardar_cambiar_caracteristica" id="cuantos_hijos" name="cuantos_hijos"  value="{{$caracteristicas_valor['cuantos_hijos']}}" autocomplete="nope">
            </div>
            <div class="form-group col-md-4">
                <label for="padre_cabeza">¿Es padre o madre cabeza de familia?</label>
                <span style="display:none; color:red;" id="padre_cabeza--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="padre_cabeza" name="padre_cabeza" value="{{$caracteristicas_valor['padre_cabeza']}}">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['padre_cabeza'] == 'Si' ? 'selected' : '' }} value="Si">Si</option>
                    <option {{$caracteristicas_valor['padre_cabeza'] == 'No' ? 'selected' : '' }} value="No">No</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="grupo_etnico">¿Cual es su grupo Etnico?</label>
                <span style="display:none; color:red;" id="grupo_etnico--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="grupo_etnico" name="grupo_etnico">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'mestizo' ? 'selected' : ''}} value="mestizo">mestizo</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'blancos' ? 'selected' : ''}} value="blancos">blancos</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'indio' ? 'selected' : ''}} value="indio">indio</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'mulato' ? 'selected' : ''}} value="mulato">mulato</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'zambo' ? 'selected' : ''}} value="zambo">zambo</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'negro' ? 'selected' : ''}} value="negro">negro</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'raizal' ? 'selected' : ''}} value="raizal">raizal</option>
                    <option {{$caracteristicas_valor['grupo_etnico'] == 'otro' ? 'selected' : ''}} value="otro">otro</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="presenta_limitacion_fisica">¿Presenta alguna limitacion fisica?</label>
                <span style="display:none; color:red;" id="presenta_limitacion_fisica--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="presenta_limitacion_fisica" name="presenta_limitacion_fisica">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['presenta_limitacion_fisica'] == 'Si' ? 'selected' : ''}} value="Si">Si</option>
                    <option {{$caracteristicas_valor['presenta_limitacion_fisica'] == 'No' ? 'selected' : ''}} value="No">No</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="cual_limitacion">¿Cual limitacion fisica presenta?</label>
                <span style="display:none; color:red;" id="cual_limitacion--error">Por favor ingrese un valor</span>
                <input type="text" class="form-control guardar_cambiar_caracteristica" id="cual_limitacion" name="cual_limitacion"  value="{{$caracteristicas_valor['cual_limitacion']}}" autocomplete="nope">
            </div>
            <div class="form-group col-md-4">
                <label for="tipo_vinculacion">Tipo de vinculacion</label>
                <span style="display:none; color:red;" id="tipo_vinculacion--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="tipo_vinculacion" name="tipo_vinculacion">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['tipo_vinculacion'] == 'Planta' ? 'selected' : ''}} value="Planta">Planta</option>
                    <option {{$caracteristicas_valor['tipo_vinculacion'] == 'Prestacion de servicios' ? 'selected' : ''}} value="Prestacion de servicios">Prestacion de servicios</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="nivel_academico">Nivel academico</label>
                <span style="display:none; color:red;" id="nivel_academico--error">Por favor ingrese un valor</span>

                <select class="form-control guardar_cambiar_caracteristica" id="nivel_academico" name="nivel_academico">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['nivel_academico'] == 'Auxiliar' ? 'selected' : ''}} value="Auxiliar">Auxiliar</option>
                    <option {{$caracteristicas_valor['nivel_academico'] == 'Técnico' ? 'selected' : ''}} value="Técnico">Técnico</option>
                    <option {{$caracteristicas_valor['nivel_academico'] == 'tecnólogo' ? 'selected' : ''}} value="tecnólogo">tecnólogo</option>
                    <option {{$caracteristicas_valor['nivel_academico'] == 'profesional' ? 'selected' : ''}} value="profesional">profesional</option>
                    <option {{$caracteristicas_valor['nivel_academico'] == 'profesional especializado' ? 'selected' : ''}} value="profesional especializado">profesional especializado</option>
                    <option {{$caracteristicas_valor['nivel_academico'] == 'otro' ? 'selected' : ''}} value="otro">otro</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="cargo">Cargo o actividad desempeñada</label>
                <span style="display:none; color:red;" id="cargo--error">Por favor ingrese un valor</span>
                <input type="text" class="form-control guardar_cambiar_caracteristica" id="cargo" name="cargo"  value="{{$caracteristicas_valor['cargo']}}" autocomplete="nope">
            </div>
            <div class="form-group col-md-4">
                <label for="perfil">Perfil</label>
                <span style="display:none; color:red;" id="perfil--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="perfil" name="perfil">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['perfil'] == 'Administrativo' ? 'selected' : ''}} value="Administrativo">Administrativo</option>
                    <option {{$caracteristicas_valor['perfil'] == 'Asistencial' ? 'selected' : ''}} value="Asistencial">Asistencial</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="sede">Sede</label>
                <span style="display:none; color:red;" id="sede--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="sede" name="sede" value="{{$caracteristicas_valor['sede']}}">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Zona Franca.' ? 'selected' : ''}} value="CAPS Zona Franca.">CAPS Zona Franca.</option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Pablo VI Bosa.' ? 'selected' : ''}} value="CAPS Pablo VI Bosa.">CAPS Pablo VI Bosa. </option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Bosa Centro.' ? 'selected' : ''}} value="CAPS Bosa Centro.">CAPS Bosa Centro. </option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS El Porvenir.' ? 'selected' : ''}} value="CAPS El Porvenir.">CAPS El Porvenir. </option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Cabañas' ? 'selected' : ''}} value="CAPS Cabañas.">CAPS Cabañas.</option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Trinidad Galán.' ? 'selected' : ''}} value="CAPS Trinidad Galán.">CAPS Trinidad Galán. </option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Occidente de Kennedy' ? 'selected' : ''}} value="CAPS Occidente de Kennedy.">CAPS Occidente de Kennedy.</option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Patio Bonito Tintal' ? 'selected' : ''}} value="CAPS Patio Bonito Tintal.">CAPS Patio Bonito Tintal.</option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Patio Bonito' ? 'selected' : ''}} value="CAPS Patio Bonito.">CAPS Patio Bonito.</option>
                    <option {{$caracteristicas_valor['sede'] == 'CAPS Patios' ? 'selected' : ''}} value="CAPS Patios.">CAPS Patios.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Fontibón' ? 'selected' : ''}} value="USS Fontibón.">USS Fontibón.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS San Pablo' ? 'selected' : ''}} value="USS San Pablo.">USS San Pablo.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Internacional' ? 'selected' : ''}} value="USS Internacional.">USS Internacional.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Puerta de Teja' ? 'selected' : ''}} value="USS Puerta de Teja.">USS Puerta de Teja.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Centro Día' ? 'selected' : ''}} value="USS Centro Día.">USS Centro Día.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Terminal Aéreo' ? 'selected' : ''}} value="USS Terminal Aéreo.">USS Terminal Aéreo.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Terminal Terrestre' ? 'selected' : ''}} value="USS Terminal Terrestre.">USS Terminal Terrestre.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Boston' ? 'selected' : ''}} value="USS Boston.">USS Boston.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Bernardino' ? 'selected' : ''}} value="USS Bernardino.">USS Bernardino.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS José María Carbonell' ? 'selected' : ''}} value="USS José María Carbonell.">USS José María Carbonell.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS La Estación' ? 'selected' : ''}} value="USS La Estación.">USS La Estación.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Laureles' ? 'selected' : ''}} value="USS Laureles.">USS Laureles.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Olarte' ? 'selected' : ''}} value="USS Olarte.">USS Olarte.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Palestina' ? 'selected' : ''}} value="USS Palestina.">USS Palestina.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Piamonte' ? 'selected' : ''}} value="USS Piamonte.">USS Piamonte.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Nuevas Delicias' ? 'selected' : ''}} value="USS Nuevas Delicias.">USS Nuevas Delicias.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Bosa II' ? 'selected' : ''}} value="USS Bosa II.">USS Bosa II.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Asunción Bochica' ? 'selected' : ''}} value="USS Asunción Bochica.">USS Asunción Bochica.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Cundinamarca' ? 'selected' : ''}} value="USS Cundinamarca.">USS Cundinamarca.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Alcalá Muzú' ? 'selected' : ''}} value="USS Alcalá Muzú.">USS Alcalá Muzú.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Pio XII' ? 'selected' : ''}} value="USS Pio XII.">USS Pio XII.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Mexicana' ? 'selected' : ''}} value="USS Mexicana.">USS Mexicana.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Dindalito' ? 'selected' : ''}} value="USS Dindalito.">USS Dindalito.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Britalia' ? 'selected' : ''}} value="USS Britalia.">USS Britalia.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Argelia' ? 'selected' : ''}} value="USS Argelia.">USS Argelia.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Alquería' ? 'selected' : ''}} value="USS Alquería.">USS Alquería.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Abastos' ? 'selected' : ''}} value="USS Abastos.">USS Abastos.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Bomberos' ? 'selected' : ''}} value="USS Bomberos.">USS Bomberos.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Visión Colombia' ? 'selected' : ''}} value="USS Visión Colombia.">USS Visión Colombia.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Carvajal' ? 'selected' : ''}} value="USS Carvajal.">USS Carvajal.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Class' ? 'selected' : ''}} value="USS Class.">USS Class.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Catalina' ? 'selected' : ''}} value="USS Catalina.">USS Catalina.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Floralia' ? 'selected' : ''}} value="USS Floralia.">USS Floralia.</option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Patio Bonito Tintal.' ? 'selected' : ''}} value="USS Patio Bonito Tintal.">USS Patio Bonito Tintal. </option>
                    <option {{$caracteristicas_valor['sede'] == 'USS Occidente de Kennedy' ? 'selected' : ''}} value="USS Occidente de Kennedy.">USS Occidente de Kennedy.</option>
                    <option {{$caracteristicas_valor['sede'] == 'Dirección de Gestión del Riesgo en Salud – FDL.' ? 'selected' : ''}} value="Dirección de Gestión del Riesgo en Salud – FDL.">Dirección de Gestión del Riesgo en Salud – FDL. </option>
                    <option {{$caracteristicas_valor['sede'] == 'Dirección de Gestión del Riesgo en Salud – PIC.' ? 'selected' : ''}} value="Dirección de Gestión del Riesgo en Salud – PIC.">Dirección de Gestión del Riesgo en Salud – PIC. </option>
                    <option {{$caracteristicas_valor['sede'] == 'Dirección de Gestión del Riesgo en Salud – PYD' ? 'selected' : ''}} value="Dirección de Gestión del Riesgo en Salud – PYD.">Dirección de Gestión del Riesgo en Salud – PYD.</option>
                    <option {{$caracteristicas_valor['sede'] == 'Sede Administrativa – Asdincgo.' ? 'selected' : ''}} value="Sede Administrativa – Asdincgo. ">Sede Administrativa – Asdincgo. </option>
                    <option {{$caracteristicas_valor['sede'] == 'Sede Administrativa - Cundinamarca / Talento Humano.' ? 'selected' : ''}} value="Sede Administrativa - Cundinamarca / Talento Humano.">Sede Administrativa - Cundinamarca / Talento Humano. </option>
                    <option {{$caracteristicas_valor['sede'] == 'Sede Administrativa - Pablo VI Bosa / Centro de Servicios Integrales.' ? 'selected' : ''}} value="Sede Administrativa - Pablo VI Bosa / Centro de Servicios Integrales.">Sede Administrativa - Pablo VI Bosa / Centro de Servicios Integrales. </option>
                    <option {{$caracteristicas_valor['sede'] == 'Sede Administrativa - Bodega Gestión Documental / B. Carvajal' ? 'selected' : ''}} value="Sede Administrativa - Bodega Gestión Documental / B. Carvajal.">Sede Administrativa - Bodega Gestión Documental / B. Carvajal.</option>
                    <option {{$caracteristicas_valor['sede'] == 'Sede Administrativa - Carvajal / Activos Fijos' ? 'selected' : ''}} value="Sede Administrativa - Carvajal / Activos Fijos.">Sede Administrativa - Carvajal / Activos Fijos.</option>
                    <option {{$caracteristicas_valor['sede'] == 'Sede Administrativa – Bodega Montevideo' ? 'selected' : ''}} value="Sede Administrativa – Bodega Montevideo.">Sede Administrativa – Bodega Montevideo.</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="servicio">Servicio</label>
                <span style="display:none; color:red;" id="servicio--error">Por favor ingrese un valor</span>
                <input type="text" class="form-control guardar_cambiar_caracteristica" id="servicio" name="servicio"  value="{{$caracteristicas_valor['servicio']}}" autocomplete="nope">
            </div>
            <div class="form-group col-md-4">
                <label for="proceso_pertenece">¿A cual proceso pertenece?</label>
                <span style="display:none; color:red;" id="proceso_pertenece--error">Por favor ingrese un valor</span>
                <input type="text" class="form-control guardar_cambiar_caracteristica" id="proceso_pertenece" name="proceso_pertenece"  value="{{$caracteristicas_valor['proceso_pertenece']}}" autocomplete="nope">
            </div>
            <div class="form-group col-md-4">
                <label for="lider">¿Es lider de Area o de equipo de trabajo?</label>
                <span style="display:none; color:red;" id="lider--error">Por favor ingrese un valor</span>
                <select class="form-control guardar_cambiar_caracteristica" id="lider" name="lider">
                    <option value="">Seleccione una opcion</option>
                    <option {{$caracteristicas_valor['lider'] == 'Si' ? 'selected' : ''}} value="Si">Si</option>
                    <option {{$caracteristicas_valor['lider'] == 'No' ? 'selected' : ''}} value="No">No</option>
                </select>
            </div>

            <div class="col-md-12">
                <button class="btn btn-danger pull-right" id="boton_informacion_sociodemografica">Continuar</button>
            </div>
        </div>
    </div>
    <div class="row col-md-10 col-md-offset-1" id="encuesta">
        @if (!$respuestasCompletas)
        <center id="titulo_encuesta">
            <h2>Encuesta de cultura organizacional</h2>
            <h3>Encuesta online</h3>
        </center>
        @endif
        <hr />
        @php
            // Se encargar de llevar un consecutivo por bloque
            $contadorBloques = 0;

            // Se encarga de contar el total de preguntas
            $contadorGlobal = 1;

            // Se encarga de contar las preguntas agrupadas
            $contadorLocal = 1;

            // Se sirve como referencia para validar la cantidad maxima de preguntas por bloque
            $cantidadMaximaBloque = 12;
        @endphp
        @if ($respuestasCompletas)
            <div class="row" tag="mensaje_finalizacion">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h4>Ya has respondido todas las preguntas.</h4>
                    <br>
                    <h2>¡Gracias por presentar la encuesta!</h2>
                    <br>
                    <br>
                    <a href="{{url('cerrar-sesion')}}" class="btn btn-danger">Salir</a>
                </div>
            </div>
        @else
            @foreach($preguntas AS $pregunta)
                @if($contadorLocal <= 1)
                    <table class="table table-striped bloques-visibles" id="bloque-{{$contadorBloques}}">
                        <thead>
                            <tr>
                                <th nowrap>ID</th>
                                <th>Pregunta</th>
                                <th nowrap>Siempre</th>
                                <th nowrap>Casi siempre</th>
                                <th nowrap>Algunas veces</th>
                                <th nowrap>Casi nunca</th>
                                <th nowrap>Nunca</th>
                            </tr>
                        </thead>
                        <tbody>
                @endif
                        <tr>
                            <td nowrap>{{$contadorGlobal}}</td>
                            <td>
                                <span style="color: red; display: none;" id="error-pregunta--{{$pregunta->id}}">Por favor seleccione una opción <br></span>
                                {{$pregunta->pregunta}}
                            </td>
                            <td nowrap align="center">
                                <input type="radio" class="guardar_cambiar_encuesta" tag="{{$pregunta->id}}" id="pregunta--{{$pregunta->id}}-1" name="pregunta--{{$pregunta->id}}" value="Siempre" {{$respuestas[$pregunta->id] == 'Siempre' ? 'checked' : ''}} />
                            </td>
                            <td nowrap align="center">
                                <input type="radio" class="guardar_cambiar_encuesta" tag="{{$pregunta->id}}" id="pregunta--{{$pregunta->id}}-2" name="pregunta--{{$pregunta->id}}" value="Casi siempre" {{$respuestas[$pregunta->id] == 'Casi siempre' ? 'checked' : ''}} />
                            </td>
                            <td nowrap align="center">
                                <input type="radio" class="guardar_cambiar_encuesta" tag="{{$pregunta->id}}" id="pregunta--{{$pregunta->id}}-3" name="pregunta--{{$pregunta->id}}" value="Algunas veces" {{$respuestas[$pregunta->id] == 'Algunas veces' ? 'checked' : ''}} />
                            </td>
                            <td nowrap align="center">
                                <input type="radio" class="guardar_cambiar_encuesta" tag="{{$pregunta->id}}" id="pregunta--{{$pregunta->id}}-4" name="pregunta--{{$pregunta->id}}" value="Casi nunca" {{$respuestas[$pregunta->id] == 'Casi nunca' ? 'checked' : ''}} />
                            </td>
                            <td nowrap align="center">
                                <input type="radio" class="guardar_cambiar_encuesta" tag="{{$pregunta->id}}" id="pregunta--{{$pregunta->id}}-5" name="pregunta--{{$pregunta->id}}" value="Nunca" {{$respuestas[$pregunta->id] == 'Nunca' ? 'checked' : ''}} />
                            </td>
                        </tr>
                @if ($contadorLocal === $cantidadMaximaBloque)
                        </tbody>
                    </table>
                @endif

                @php
                    if ($contadorLocal === $cantidadMaximaBloque) {
                        $contadorBloques++;
                        $contadorLocal = 0;
                    }
                    $contadorGlobal++;
                    $contadorLocal++;

                    if ($contadorGlobal == count($preguntas) && $contadorLocal < $cantidadMaximaBloque) {
                        $contadorBloques++;
                    }
                @endphp


            @endforeach
            <div class="row" id="contenedor_botones_encuesta">
                <button class="btn btn-danger pull-left" id="boton_encuesta_atras" disabled="true">Atras</button>
                <button class="btn btn-danger pull-right" id="boton_encuesta_continuar">Continuar</button>
            </div>
            <div class="row bloques-visibles" id="bloque-{{$contadorBloques}}" tag="mensaje_finalizacion">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h4> La encuesta ha finalizado</h4>
                    <br>
                    <h2>¡Gracias por presentar la encuesta!</h2>
                    <br>
                    <br>
                    <a href="{{url('cerrar-sesion')}}" class="btn btn-danger">Salir</a>
                </div>
            </div>
        @endif
    </div>

@endsection

@section('modal')
<div class="modal fade" id="modal_politicas">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Politicas de Privacidad y Seguridad</h4>
            </div>

            <div class="modal-body text-justify">

                {{session()->get('cliente')->politicas}}

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
@endsection
