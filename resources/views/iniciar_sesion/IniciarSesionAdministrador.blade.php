@extends('plantilla.iniciar_sesion.base')

@section('titulo', 'Iniciar sesión - Presentar')

@section('javascript')
<script type="text/javascript">
    $(document).ready(() =>
    {
        $("#cargando").hide();
        // Reveal Login form
        setTimeout(function(){ $(".fade-in-effect").addClass('in'); }, 1);


        // Validation and Ajax action
        $("form#login").validate({
            rules: {
                numero_documento: {
                    required: true
                },
                cliente_id: {
                    required: true
                },
                contrasena: {
                    required: true
                },
            },

            messages: {
                numero_documento: {
                    required: 'Por favor ingrese el correo.'
                },
                cliente_id: {
                    required: 'Por favor seleccione la compañia.'
                },
                contrasena: {
                    required: 'Por favor ingrese la contraseña.'
                },
            },

            // Form Processing via AJAX
            submitHandler: function(form)
            {
                var opts = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-full-width",
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
                };

                $.ajax({
                    url: "{{ route('iniciar-sesion/administrar') }}",
                    method: 'POST',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        numero_documento: $(form).find('#numero_documento').val(),
                        cliente_id: $(form).find('#cliente_id').val(),
                        password: $(form).find('#password').val(),
                    },
                    success: function(respuesta)
                    {
                        if(respuesta[0])
                        {
                            window.location.href = '{{route("/")}}';
                        }
                        else
                        {
                            toastr.error("Lo sentimos, su numero de documento no se encuentra registrado en nuestro sistema, por favor contacte al administrador.", opts);
                        }
                    }
                });
            }
        });

        // Set Form focus
        $("form#login .form-group:has(.form-control):first .form-control").focus();

        $("#numero_documento").blur(function() {
            if ($(this).val() !== '') {
                $("#cargando").fadeIn();

                $.ajax({
                    url: "{{ route('iniciar-sesion/obtener-clientes') }}",
                    method: 'POST',
                    dataType: 'json',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        numero_documento: $(this).val(),
                    },
                    success: function(respuesta)
                    {
                        $("#cliente_id").html('');
                        $("#cliente_id").append('<option value="">Seleccionar empresa</option>');
                        $.each(respuesta, (index, object) => {
                            if (respuesta.length <= 1) {
                                $("#cliente_id").append('<option selected value="'+object.id+'">'+object.nombre+'</option>');
                                $("#buttonSubmit").removeProp('disabled')
                            } else {
                                $("#cliente_id").append('<option value="'+object.id+'">'+object.nombre+'</option>');
                            }
                        });
                        $("#cargando").fadeOut();
                    }
                });
            } else {
                $("#cliente_id").html('');
                $("#cliente_id").append('<option value="">Seleccionar empresa</option>');
            }

        });

        $("#cliente_id").change(function() {
            if ($(this).val() !== '') {
                $("#buttonSubmit").removeProp('disabled')
            } else {
                $("#buttonSubmit").prop('disabled', 'true')
            }
        })
    });
</script>
@endsection

@section('contenido')
<!-- Add class "fade-in-effect" for login form effect -->
<form method="post" role="form" id="login" class="login-form fade-in-effect">

    <div class="login-header text-center">
        <a href="{{url('/')}}" class="logo">
            <img src="{{asset('imagenes/default/logo.png')}}" alt="" width="80" />

        </a>
        <span>Iniciar sesión</span>

        <p>Apreciado usuario, esta apunto de ingresar a la zona de administracion de la plataforma.</p>
    </div>

    <div class="form-group">
        <label class="control-label" for="numero_documento">Numero de documento</label>
        <input type="number" class="form-control" name="numero_documento" id="numero_documento" autocomplete="off" />
    </div>

    <div class="form-group">
        <label class="control-label" for="password">Contraseña</label>
        <input type="password" class="form-control" name="password" id="password" autocomplete="off" />
    </div>

    <div class="form-group">
        <select class="form-control" name="cliente_id" id="cliente_id">
            <option value="">Seleccionar empresa</option>
        </select>
    </div>
    <div id="cargando">Cargando...</div>

    <div class="form-group">
        <button type="submit" id="buttonSubmit" class="btn btn-dark  btn-block text-left" disabled="true">
            <i class="fa-lock"></i>
            Ingresar
        </button>
    </div>

    <div class="login-footer">

        <div class="info-links">
            <a href="#">Politica de privacidad</a>
        </div>

        <div class="info-links">
            <a href="{{route('iniciar-sesion', 'presentar')}}">Iniciar sesion en la zona de formularios</a>
        </div>

    </div>

</form>

<!-- External login -->
   <!--
<div class="external-login">
    <a href="#" class="facebook">
        <i class="fa-facebook"></i>
        Facebook Login
    </a>


    <a href="#" class="twitter">
        <i class="fa-twitter"></i>
        Login with Twitter
    </a>

    <a href="#" class="gplus">
        <i class="fa-google-plus"></i>
        Login with Google Plus
    </a>
</div>
-->
@endsection
