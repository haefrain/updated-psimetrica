<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="@yield('titulo')" />
    <meta name="author" content="" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('titulo')</title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="{{asset('css/fonts/linecons/css/linecons.css')}}">
	<link rel="stylesheet" href="{{asset('css/fonts/fontawesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('css/xenon-core.css')}}">
	<link rel="stylesheet" href="{{asset('css/xenon-forms.css')}}">
	<link rel="stylesheet" href="{{asset('css/xenon-components.css')}}">
	<link rel="stylesheet" href="{{asset('css/xenon-skins.css')}}">
	<link rel="stylesheet" href="{{asset('css/custom.css')}}">

	<script src="{{asset('js/jquery-1.11.1.min.js')}}"></script>

</head>
<body class="page-body login-page login-light">

    <div class="page-loading-overlay">
        <div class="loader-3"></div>
    </div>

	<div class="login-container">

		<div class="row">

			<div class="col-sm-6">



				<!-- Errors container -->
				<div class="errors-container">


				</div>

				@yield('contenido')

			</div>

		</div>

    </div>

    @yield('javascript')

	<!-- Bottom Scripts -->
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<script src="{{asset('js/TweenMax.min.js')}}"></script>
	<script src="{{asset('js/resizeable.js')}}"></script>
	<script src="{{asset('js/joinable.js')}}"></script>
	<script src="{{asset('js/xenon-api.js')}}"></script>
	<script src="{{asset('js/xenon-toggles.js')}}"></script>
	<script src="{{asset('js/jquery-validate/jquery.validate.min.js')}}"></script>
	<script src="{{asset('js/toastr/toastr.min.js')}}"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="{{asset('js/xenon-custom.js')}}"></script>

</body>
</html>
