@extends('plantilla.administrador.base')

@section('titulo', 'Configuracion - Clientes')

@section('javascript')
@endsection

@section('contenido')
    @if(!isset($cliente))
        @php
            $cliente = [
                'nit' => old('nit'),
                'nombre' => old('nombre'),
                'logo' => old('logo'),
                'politicas' => old('politicas'),
                'direccion' => old('direccion'),
                'telefono' => old('telefono'),
                'celular' => old('celular'),
                'representante' => old('representante'),
                'telefono_representante' => old('telefono_representante'),
                'correo' => old('correo'),
                'method' => 'POST',
                'url' => url('configuracion/cliente')
            ];
        @endphp
    @else
        @php
            $cliente = $cliente->toArray();
            $cliente['method'] = 'PUT';
            $cliente['url'] = url('configuracion/cliente/'.$cliente['id']);
        @endphp
    @endif
    @include('configuracion.cliente.formulario', $cliente)
@endsection
