<form method="POST" action="{{$cliente['url']}}">
    @csrf
    @method($cliente['method'])
    <div class="form-group col-md-6">
        <label for="NIT">NIT:</label>
        <input type="text" class="form-control" id="NIT" name="nit" value="{{$cliente['nit']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" id="nombre" name="nombre" value="{{$cliente['nombre']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="logo">Logo:</label>
        <input type="file" class="form-control" id="logo" name="logo" value="{{$cliente['logo']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="politicas">Politicas:</label>
        <textarea type="text" class="form-control" id="politicas" name="politicas"></textarea value="{{$cliente['politicas']}}">
    </div>

    <!--<div class="form-group col-md-6">
        <label for="tema">tema:</label>

        <input type="text" class="form-control" id="tema" name="tema" value="">
    </div>-->

    <div class="form-group col-md-6">
        <label for="direccion">Dirección:</label>
        <input type="text" class="form-control" id="direccion" name="direccion" value="{{$cliente['direccion']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="telefono">Telefono:</label>
        <input type="text" class="form-control" id="telefono" name="telefono" value="{{$cliente['telefono']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="celular">Celular:</label>
        <input type="text" class="form-control" id="celular" name="celular" value="{{$cliente['celular']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="representante">Representante:</label>
        <input type="text" class="form-control" id="representante" name="representante" value="{{$cliente['representante']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="telefono_representante">Telefono de representante:</label>
        <input type="text" class="form-control" id="telefono_representante" name="telefono_representante" value="{{$cliente['telefono_representante']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="correo">Correo:</label>
        <input type="email" class="form-control" id="correo" name="correo" value="{{$cliente['correo']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="crear_usuario">
            <input type="checkbox" class="cbr" id="crear_usuario" name="crear_usuario">
            ¿Desea crear un usuario con los datos del representante?
        </label>

    </div>
    <div class="form-group col-md-12">
        <hr />
    </div>

    <div class="form-group col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-danger btn-single pull-right">Guardar</button>
            <a href="{{url('configuracion/cliente')}}" class="btn btn-warning btn-single pull-left">Cancelar</a>
        </div>
    </div>

</form>
