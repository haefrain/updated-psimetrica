@extends('plantilla.administrador.base')

@section('titulo', 'Configuracion - Roles')

@section('javascript')
@endsection

@section('contenido')
    @if(!isset($rol))
        @php
            $rol = [
                'nombre' => old('nombre'),
                'method' => 'POST',
                'url' => url('configuracion/rol')
            ];
        @endphp
    @else
        @php
            $rol = $rol->toArray();
            $rol['method'] = 'PUT';
            $rol['url'] = url('configuracion/rol/'.$rol['id']);
        @endphp
    @endif
    @include('configuracion.rol.formulario', $rol)
@endsection
