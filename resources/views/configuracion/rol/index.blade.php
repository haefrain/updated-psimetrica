@extends('plantilla.administrador.base')

@section('titulo', 'Configuracion - Roles')

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($)
        {
            $("#tabla-roles").dataTable({
                aLengthMenu: [
                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]
                ]
            });
        });
    </script>
@endsection

@section('contenido')
<div class="panel-heading">
    <h2 class="panel-title">Roles</h2>

    <div class="panel-options">
        <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
                Opciones <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-blue" role="menu">
                <li>
                    <a href="{{url('configuracion/rol/create')}}">Crear</a>
                </li>
                <li>
                    <a href="#">Exportar</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="panel-body">
    <div class="table-responsive" style="padding: 1em;">
            <table id="tabla-roles" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles AS $rol)
                    <tr>
                        <td>{{$rol->nombre}}</td>
                        <td>
                            <div class="vertical-top">
                                <a href='{{url("configuracion/rol/$rol->id/edit")}}'' class="btn btn-icon btn-blue">
                                    <i class="fa-pencil"></i>
                                </a>
                                <form action='{{url("configuracion/rol/$rol->id")}}' method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"  class="btn btn-icon btn-red">
                                        <i class="fa-remove"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection
