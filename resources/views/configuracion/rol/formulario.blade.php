<form method="POST" action="{{$rol['url']}}">
    @csrf
    @method($rol['method'])

    <div class="form-group col-md-6">
        <label for="nombre">Nombre:</label>
        <input type="text" class="form-control" id="nombre" name="nombre" value="{{$rol['nombre']}}">
    </div>

    <div class="form-group col-md-12">
        <hr />
    </div>

    <div class="form-group col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-danger btn-single pull-right">Guardar</button>
            <a href="{{url('configuracion/rol')}}" class="btn btn-warning btn-single pull-left">Cancelar</a>
        </div>
    </div>

</form>
