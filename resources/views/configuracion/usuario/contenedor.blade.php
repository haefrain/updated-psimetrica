@extends('plantilla.administrador.base')

@section('titulo', 'Configuracion - Usuarios')

@section('javascript')
@endsection

@section('contenido')
    @if(!isset($usuario))
        @php
            $usuario = [
                'name' => old('name'),
                'email' => old('email'),
                'numero_documento' => old('numero_documento'),
                'password' => old('password'),
                'method' => 'POST',
                'url' => url('configuracion/usuario')
            ];
        @endphp
    @else
        @php
            $usuario = $usuario->toArray();
            $usuario['method'] = 'PUT';
            $usuario['url'] = url('configuracion/usuario/'.$usuario['id']);
        @endphp
    @endif
    @include('configuracion.usuario.formulario', $usuario)
@endsection
