@extends('plantilla.administrador.base')

@section('titulo', 'Configuracion - Usuarios')

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function($)
        {
            $("#tabla-usuarios").dataTable({
                aLengthMenu: [
                    [10, 25, 50, 100, -1], [10, 25, 50, 100, "Todos"]
                ]
            });
        });
    </script>
@endsection

@section('contenido')
<div class="panel-heading">
    <h2 class="panel-title">Usuarios</h2>

    <div class="panel-options">
        <div class="btn-group">
            <button type="button" class="btn btn-blue dropdown-toggle" data-toggle="dropdown">
                Opciones <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-blue" role="menu">
                <li>
                    <a href="{{url('configuracion/usuario/create')}}">Crear</a>
                </li>
                <li>
                    <a href="#">Exportar</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="panel-body">
    <div class="table-responsive" style="padding: 1em;">
            <table id="tabla-usuarios" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Numero de documento</th>
                        <th>Nombre</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($usuarios AS $usuario)
                    <tr>
                        <td>{{$usuario->numero_documento}}</td>
                        <td>{{$usuario->name}}</td>
                        <td>
                            <div class="vertical-top">
                                <a href='{{url("configuracion/usuario/$usuario->id/edit")}}'' class="btn btn-icon btn-blue">
                                    <i class="fa-pencil"></i>
                                </a>
                                <form action='{{url("configuracion/usuario/$usuario->id")}}' method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"  class="btn btn-icon btn-red">
                                        <i class="fa-remove"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
    </div>
</div>
@endsection
