<form method="POST" action="{{$usuario['url']}}">
    @csrf
    @method($usuario['method'])
    <div class="form-group col-md-6">
        <label for="name">Nombre:</label>
        <input type="text" class="form-control" id="name" name="name" value="{{$usuario['name']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="numero_documento">Numero de documento:</label>
        <input type="number" class="form-control" id="numero_documento" name="numero_documento" value="{{$usuario['numero_documento']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="logo">Correo electronico:</label>
        <input type="email" class="form-control" id="email" name="email" value="{{$usuario['email']}}">
    </div>

    <div class="form-group col-md-6">
        <label for="password">Contraseña:</label>
        <input type="password" class="form-control" id="password" name="password">
    </div>

    <div class="form-group col-md-12">
        <hr />
    </div>

    <div class="form-group col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-danger btn-single pull-right">Guardar</button>
            <a href="{{url('configuracion/usuario')}}" class="btn btn-warning btn-single pull-left">Cancelar</a>
        </div>
    </div>

</form>
