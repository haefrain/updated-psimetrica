<?php

namespace App\Http\Controllers\Proceso;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use DB;
use App\User;
use App\Models\Participante;

class ProcesoController extends Controller
{
    public function index()
    {
        $dato = [
            'name' => 'Monica Caro',
            'email' => 'gerencia@psinapsisoutsourcing.com',
            'numero_documento' => '12345678',
            'password' => bcrypt('12345678')
        ];

        $usuario = User::create($dato);

        $roles_usuario = [
            'role_id' => 5,
            'user_id' => $usuario->id,
            'cliente_id' => 3
        ];
        DB::table('roles_usuarios')->insert($roles_usuario);

        $participante = [
            'proceso_id' => 1,
            'usuario_id' => $usuario->id
        ];

        $participanteData = Participante::create($participante);

        $dataCaracteristicas = [
            [
                'caracteristica_id' => 1,
                'participante_id' => $participanteData->id,
                'valor' => $dato['numero_documento'],
            ],
            [
                'caracteristica_id' => 2,
                'participante_id' => $participanteData->id,
                'valor' => $dato['name'],
            ],
            [
                'caracteristica_id' => 3,
                'participante_id' => $participanteData->id,
                'valor' => $dato['email'],
            ]
        ];

        DB::table('participantes_caracteristicas')->insert($dataCaracteristicas);

        $subject = "Registro en Psimetrica";
        $for = $usuario->email;
        Mail::send('correos.registro', $dato, function ($msj) use ($subject, $for) {
            $msj->from("info@psimetrica.com", "Psimetrica");
            $msj->subject($subject);
            $msj->to($for);
        });
    }
}
