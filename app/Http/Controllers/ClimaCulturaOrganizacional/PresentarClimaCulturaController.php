<?php

namespace App\Http\Controllers\ClimaCulturaOrganizacional;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class PresentarClimaCulturaController extends Controller
{
    public function index()
    {
        $caracteristicas_participante = DB::table('participantes AS P')
            ->select('C.caracteristica', 'PC.valor')
            ->leftJoin('participantes_caracteristicas AS PC', 'PC.participante_id', '=', 'P.id')
            ->leftJoin('caracteristicas AS C', 'C.id', '=', 'PC.caracteristica_id')
            ->where(['P.usuario_id' => Auth()->getUser()->id])
            ->get();

        $caracteristicas = DB::table('caracteristicas')->get();
        $caracteristicas_valor = [];
        foreach ($caracteristicas as $caracteristica) {
            $caracteristicas_valor[$caracteristica->caracteristica] = '';
        }
        foreach ($caracteristicas_participante as $participante) {
            $caracteristicas_valor[$participante->caracteristica] = $participante->valor;
        }

        $instrumento = DB::table('instrumentos')->first();
        $preguntas = DB::table('categorias')
            ->select('preguntas.categoria_id', 'preguntas.id', 'preguntas.descripcion AS pregunta', 'categorias.nombre AS categoria')
            ->join('preguntas', 'preguntas.categoria_id', '=', 'categorias.id')
            ->where('instrumento_id', $instrumento->id)
            ->get();

        $respuestas = [];
        foreach ($preguntas as $pregunta) {
            $respuestas[$pregunta->id] = '';
        }

        $respuestasRegistradas = DB::table('respuestas')
            ->join('participantes', 'respuestas.participante_id', '=', 'participantes.id')
            ->where('participantes.usuario_id', Auth()->getUser()->id)
            ->get();

        $respuestasCompletas = (count($respuestasRegistradas) === count($preguntas)) ? true : false;

        foreach ($respuestasRegistradas as $respuestaRegistrada) {
            $respuestas[$respuestaRegistrada->pregunta_id] = $respuestaRegistrada->respuesta;
        }

        $plantilla_respuestas = DB::table('plantilla_respuestas')->get();

        return view('clima_cultura_organizacional.presentar', compact('caracteristicas_valor', 'preguntas', 'plantilla_respuestas', 'respuestas', 'respuestasCompletas'));
    }

    public function store(Request $request)
    {
        $participante = DB::table('participantes')->where('usuario_id', Auth()->getUser()->id)->first();
        DB::table('respuestas')->where(['participante_id' => $participante->id, 'pregunta_id' => $request->campo])->delete();
        DB::table('respuestas')->insert(['participante_id' => $participante->id, 'pregunta_id' => $request->campo, 'respuesta' => $request->valor, 'created_at' => date('Y-m-d H:i:s')]);
        return response()->json('Guardado con exito');
    }

    public function update(Request $request, $tipo)
    {
        $campo = DB::table('caracteristicas')->where('caracteristica', $request->campo)->first();
        $participante = DB::table('participantes')->where('usuario_id', Auth()->getUser()->id)->first();
        DB::table('participantes_caracteristicas')->where(['participante_id' => $participante->id, 'caracteristica_id' => $campo->id])->delete();
        DB::table('participantes_caracteristicas')->insert(['participante_id' => $participante->id, 'caracteristica_id' => $campo->id, 'valor' => $request->valor, 'created_at' => date('Y-m-d H:i:s')]);
        return response()->json('Guardado con exito');
    }
}
