<?php

namespace App\Http\Controllers\Configuracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Configuracion\Rol;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Rol::get();
        return view('configuracion.rol.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.rol.contenedor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rol = Rol::create($request->all());
            if ($rol) {
                return redirect('configuracion/rol')->with('Exito', 'Se ha creado el registro exitosamente.');
            } else {
                return redirect()->back()->with('Error', 'Oh no, ha ocurrido un error durante la creación del rol.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = Rol::find($id);
        return view('configuracion.rol.contenedor', compact('rol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $rol = Rol::find($id)->update($request->all());
            if ($rol) {
                return redirect('configuracion/rol')->with('Exito', 'Se ha actualizado el registro exitosamente.');
            } else {
                return redirect()->back()->with('Error', 'Oh no, ha ocurrido un error al intentar actualizar el registro.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Rol::find($id)->delete();
            return redirect('configuracion/rol')->with('Exito', 'Se ha eliminado el registro correctamente.');
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }
}
