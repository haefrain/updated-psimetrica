<?php

namespace App\Http\Controllers\Configuracion;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Mail;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::get();
        return view('configuracion.usuario.index', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.usuario.contenedor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $informacion_formulario = $request->all();
            $informacion_formulario['password'] = bcrypt($informacion_formulario['password']);
            $usuario = User::create($informacion_formulario);

            $subject = "Registro en Psimetrica";
            $for = $usuario->email;
            Mail::send('correos.registro', $request->all(), function ($msj) use ($subject, $for) {
                $msj->from("info@psimetrica.com", "Psimetrica");
                $msj->subject($subject);
                $msj->to($for);
            });
            if ($usuario) {
                return redirect('configuracion/usuario')->with('Exito', 'Se ha creado el registro exitosamente.');
            } else {
                return redirect()->back()->with('Error', 'Oh no, ha ocurrido un error durante la creación del usuario.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = User::find($id);
        return view('configuracion.usuario.contenedor', compact('usuario'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $informacion_formulario = $request->all();
            $informacion_formulario['password'] = bcrypt($informacion_formulario['password']);
            $usuario = User::find($id)->update($informacion_formulario);
            if ($usuario) {
                return redirect('configuracion/usuario')->with('Exito', 'Se ha actualizado el registro exitosamente.');
            } else {
                return redirect()->back()->with('Error', 'Oh no, ha ocurrido un error al intentar actualizar el registro.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $usuario = User::find($id)->delete();
            if ($usuario) {
                return redirect('configuracion/usuario')->with('Exito', 'Se ha eliminado el registro correctamente.');
            } else {
                return redirect('configuracion/usuario')->with('Error', 'Oh no, ha ocurrido un error al eliminar el registro.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }
}
