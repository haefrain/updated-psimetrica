<?php

namespace App\Http\Controllers\Configuracion;

use Illuminate\Http\Request;
use App\Models\Configuracion\Cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::get();
        return view('configuracion.cliente.index', compact('clientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('configuracion.cliente.contenedor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $cliente = Cliente::create($request->all());
            if ($cliente) {
                return redirect('configuracion/cliente')->with('Exito', 'Se ha creado el registro exitosamente.');
            } else {
                return redirect()->back()->with('Error', 'Oh no, ha ocurrido un error durante la creación del cliente.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        return view('configuracion.cliente.contenedor', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cliente = Cliente::find($id)->update($request->all());
            if ($cliente) {
                return redirect('configuracion/cliente')->with('Exito', 'Se ha actualizado el registro exitosamente.');
            } else {
                return redirect()->back()->with('Error', 'Oh no, ha ocurrido un error al intentar actualizar el registro.');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Cliente::find($id)->delete();
            return redirect('configuracion/cliente')->with('Exito', 'Se ha eliminado el registro correctamente.');
        } catch (\Throwable $th) {
            return redirect()->back()->with('Error', $th);
        }
    }
}
