<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Configuracion\Cliente;
use Auth;
use DB;

class IniciarSesionController extends Controller
{
    public function VistaIniciarSesion($tipo)
    {
        switch ($tipo) {
            case 'administrador':
                return view('iniciar_sesion.IniciarSesionAdministrador');
            case 'presentar':
                return view('iniciar_sesion.IniciarSesionPresentar');
            default:
                return view('iniciar_sesion.IniciarSesionAdministrador');
        }
    }

    public function IniciarSesionPresentar(Request $request)
    {
        $respuesta = false;
        try {
            $userId = User::select('users.id')->join('roles_usuarios', 'roles_usuarios.user_id', '=', 'users.id')->where('numero_documento', $request->numero_documento)->where('role_id', '>', 4)->firstOrFail();
            $cliente = Cliente::find($request->cliente_id);
            if (Auth::loginUsingId($userId->id)) {
                session(['cliente' => $cliente, 'entorno' => 'Presentacion']);
                $respuesta = true;
            } else {
                $respuesta = false;
            }

            return response()->json([$respuesta]);
        } catch (\Throwable $th) {
            return response()->json([false]);
        }
    }

    public function IniciarSesionAdministrar(Request $request)
    {
        $respuesta = false;
        try {
            $credentials = $request->only('numero_documento', 'password');

            if (Auth::attempt($credentials)) {
                $respuesta = true;
            } else {
                $respuesta = false;
            }

            return response()->json([$respuesta]);
        } catch (\Throwable $th) {
            return response()->json([false]);
        }
    }

    public function CerrarSesion()
    {
        session()->forget('Modulo');
        session()->forget('cliente');
        Auth::logout();
        return redirect('iniciar-sesion/presentar');
    }

    public function ObtenerClientes(Request $request)
    {
        $numero_documento = $request->numero_documento;
        $usuario = User::where('numero_documento', $numero_documento)->first();
        return response()->json($usuario->ObtenerClientes->unique());
    }
}
