<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class InicioController extends Controller
{
    public function index()
    {
        //Obtenemos los modulo
        $modulos = $this->obtenerModulos();
        //Retornamos la vista
        return view('inicio', compact('modulos'));
    }

    public function obtenerModulos()
    {
        $modulos = array();

        $usuario = Auth()->getUser();

        foreach ($usuario->ObtenerRoles as $role) {
            foreach ($role->ObtenerPermisos as $permiso) {
                $modulo = $permiso->ObtenerGrupoPermiso->ObtenerModulo;
                if ($modulo->tipo != 'Logico') {
                    $modulos[$modulo->nombre] = $modulo;
                }
            }
        }
        return $modulos;
    }

    public function obtenerMenu($modulo_id)
    {
        $grupo_permisos = array();

        $usuario = Auth()->getUser();

        if (session()->get('entorno')) {
            $tipo = session()->get('entorno');
        } else {
            $tipo = 'Administrativo';
        }

        foreach ($usuario->ObtenerRoles as $role) {
            foreach ($role->ObtenerPermisos as $permiso) {
                $grupo_permiso = $permiso->ObtenerGrupoPermiso;
                if (
                    $grupo_permiso->modulo_id == $modulo_id &&
                    $grupo_permiso->url !== '' &&
                    $grupo_permiso->tipo === $tipo
                ) {
                    $grupo_permisos[$grupo_permiso->nombre] = $grupo_permiso;
                }
            }
        }
        if (count($grupo_permisos) === 1 && $tipo === 'Presentacion') {
            return redirect('clima-cultura-organizacional/' . $grupo_permisos['Presentar encuesta']->url);
        }
        return $grupo_permisos;
    }

    public function seleccionarModulo($modulo_id)
    {
        session()->put('Modulo', $modulo_id);
        if (session()->get('entorno') === 'Presentacion') {
            return redirect('clima-cultura-organizacional/presentar-encuesta');
        }
        return redirect('/');
    }
}
