<?php

namespace App\Http\Middleware;

use Auth;

use Closure;

class CheckPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = Auth()->getUser();
        $arrUrl = explode('\\', $request->route()->getAction()['controller']);
        $arrControladorMetodo = explode('@', $arrUrl[count($arrUrl) - 1]);

        foreach ($usuario->ObtenerRoles[0]->ObtenerPermisos as $permiso) {
            if ($permiso->controlador == $arrControladorMetodo[0] && $permiso->metodo == $arrControladorMetodo[1]) {
                return $next($request);
            }
        }

        return redirect('/')->with('Error', 'No tiene permisos para realizar esta accion');
    }
}
