<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'numero_documento'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //protected $attributes = [
    //    'cliente_id' => 1,
    //];

    public function ObtenerClientes()
    {
        return $this->belongsToMany('App\Models\Configuracion\Cliente', 'roles_usuarios')
            ->withPivot('cliente_id');
    }

    public function ObtenerRoles()
    {
        return $this->belongsToMany('App\Models\Configuracion\Rol', 'roles_usuarios', 'user_id', 'role_id')
            ->withPivot('role_id');
    }
}
