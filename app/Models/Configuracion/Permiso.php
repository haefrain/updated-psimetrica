<?php

namespace App\Models\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permiso extends Model
{
    use SoftDeletes;

    protected $table = 'permisos';

    protected $fillable = ['nombre', 'url', 'controlador', 'metodo'];

    public function ObtenerGrupoPermiso()
    {
        return $this->belongsTo('App\Models\Configuracion\GrupoPermiso', 'grupo_permiso_id');
    }
}
