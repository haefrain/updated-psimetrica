<?php

namespace App\Models\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modulo extends Model
{

    use SoftDeletes;

    protected $table = 'modulos';

    protected $fillable = ['nombre', 'url'];

    public function ObtenerGrupoPermisos()
    {
        return $this->hasMany('App\Models\Configuracion\GrupoPermiso', 'modulo_id');
    }
}
