<?php

namespace App\Models\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;

    protected $table = 'clientes';

    protected $fillable = ['nombre', 'nit', 'logo', 'politicas', 'tema', 'direccion', 'telefono', 'celular', 'representante', 'telefono_representante', 'correo'];

    function ObtenerUsuarios()
    {
        return $this->belongsToMany('App\User', 'roles_usuarios')
            ->withPivot('user_id');
    }
}
