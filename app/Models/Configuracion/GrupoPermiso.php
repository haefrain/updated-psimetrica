<?php

namespace App\Models\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrupoPermiso extends Model
{

    use SoftDeletes;

    protected $table = 'grupo_permisos';

    protected $fillable = ['nombre', 'url'];

    public function ObtenerPermisos()
    {
        return $this->hasMany('App\Models\Configuracion\Permiso', 'grupo_permiso_id');
    }

    public function ObtenerModulo()
    {
        return $this->belongsTo('App\Models\Configuracion\Modulo', 'modulo_id');
    }
}
