<?php

namespace App\Models\Configuracion;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rol extends Model
{
    use SoftDeletes;

    protected $table = 'roles';

    protected $fillable = ['nombre'];

    public function ObtenerUsuarios()
    {
        return $this->belongsToMany('App\User', 'roles_usuarios', 'role_id', 'user_id')
            ->withPivot('user_id');
    }


    public function ObtenerPermisos()
    {
        return $this->belongsToMany('App\Models\Configuracion\Permiso', 'roles_permisos', 'role_id', 'permiso_id')
            ->withPivot('permiso_id');
    }
}
