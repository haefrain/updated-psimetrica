<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParticipanteCaracteristica extends Model
{
    protected $table = 'participantes_caracteristicas';
    protected $fillable = ['valor'];
}
