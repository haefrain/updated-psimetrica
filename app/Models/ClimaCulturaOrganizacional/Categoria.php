<?php

namespace App\Models\ClimaCulturaOrganizacional;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'categorias';
    protected $fillable = ['nombre', 'descripcion'];
}
