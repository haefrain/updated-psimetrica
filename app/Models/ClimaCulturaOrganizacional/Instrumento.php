<?php

namespace App\Models\ClimaCulturaOrganizacional;

use Illuminate\Database\Eloquent\Model;

class Instrumento extends Model
{
    protected $table = 'instrumentos';
    protected $fillable = ['titulo', 'descripcion', 'mostrar_politicas'];
}
