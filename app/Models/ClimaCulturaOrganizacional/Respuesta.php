<?php

namespace App\Models\ClimaCulturaOrganizacional;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table = 'respuestas';
    protected $fillable = ['respuesta'];
}
