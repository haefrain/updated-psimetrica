<?php

namespace App\Models\ClimaCulturaOrganizacional;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $table = 'preguntas';
    protected $fillable = ['descripcion'];
}
