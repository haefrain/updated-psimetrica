<?php

namespace App\Models\ClimaCulturaOrganizacional;

use Illuminate\Database\Eloquent\Model;

class PlantillaRespuesta extends Model
{
    protected $table = 'plantilla_respuestas';
    protected $fillable = ['valor'];
}
